use anchor_lang::prelude::*;
use anchor_lang::solana_program::{program::invoke_signed, system_program};
use anchor_spl::token::{self, CloseAccount, Mint, SetAuthority, TokenAccount, Transfer};
use spl_token::instruction::AuthorityType;

pub mod amm_instructions;

declare_id!("HSF658QaHvyK9khua3rv432Kv3WnY3vQGH1iSyjgUGof");

const VAULT_PDA_SEED: &[u8] = b"vault_seed";

mod empty {
    use super::*;
    declare_id!("HJt8Tjdsc9ms9i4WCZEzhzr4oyf3ANcdzXrNdLPFqm3M");
}

#[program]
pub mod khronos {
    use super::*;

    pub fn initialize_tests(_ctx: Context<InitializeTests>) -> ProgramResult {
        Ok(())
    }

    pub fn initialize_vault(
        ctx: Context<InitializeVault>,
        period: u8,
        percentil: u8,
        amount: u64,
        deposit_bump: u8,
        destination_bump: u8,
    ) -> ProgramResult {
        msg!("Setup vault");
        let vault = &mut ctx.accounts.vault;

        vault.authority = *ctx.accounts.authority.key;

        vault.deposit_mint = *ctx.accounts.deposit_mint.to_account_info().key;
        vault.destination_mint = *ctx.accounts.destination_mint.to_account_info().key;

        vault.deposit_bump = deposit_bump;
        vault.destination_bump = destination_bump;

        vault.period = period;
        vault.percentil = percentil;

        vault.is_active = true;
        vault.initial_amount = amount;

        msg!("Set accounts authority to program");

        let (vault_authority, _vault_authority_bump) =
            Pubkey::find_program_address(&[VAULT_PDA_SEED], ctx.program_id);

        token::set_authority(
            ctx.accounts.into_set_deposit_authority_context(),
            AuthorityType::AccountOwner,
            Some(vault_authority),
        )?;

        token::set_authority(
            ctx.accounts.into_set_destination_authority_context(),
            AuthorityType::AccountOwner,
            Some(vault_authority),
        )?;

        msg!("Transfer deposit");
        token::transfer(ctx.accounts.into_transfer_to_deposit_pda_context(), amount)?;

        Ok(())
    }

    pub fn close_vault(ctx: Context<CloseVault>) -> ProgramResult {
        msg!("Get vault pda");
        // Get PDA seeds to sign transactions
        let (_vault_authority, vault_authority_bump) =
            Pubkey::find_program_address(&[VAULT_PDA_SEED], ctx.program_id);
        let authority_seeds = &[&VAULT_PDA_SEED[..], &[vault_authority_bump]];

        msg!("Get withdrawn amounts");
        // Get left token amount
        let deposit_amount = ctx.accounts.deposit_account.amount;
        let destination_amount = ctx.accounts.destination_account.amount;

        // Set vault as inactive
        ctx.accounts.vault.is_active = false;

        // Check if deposit account have some tokens
        if deposit_amount > 0 {
            msg!("Withdrawn deposit");
            // Transfer tokens to withdrawn account
            token::transfer(
                ctx.accounts
                    .into_transfer_deposit_to_authority_withdrawn_acc_context()
                    .with_signer(&[&authority_seeds[..]]),
                deposit_amount,
            )?;
        }
        msg!("Close deposit account");
        // Close deposit account
        token::close_account(
            ctx.accounts
                .into_close_deposit_account_context()
                .with_signer(&[&authority_seeds[..]]),
        )?;

        // Check if destination account have some tokens
        if destination_amount > 0 {
            msg!("Withdrawn destination");
            token::transfer(
                ctx.accounts
                    .into_transfer_destination_to_authority_withdrawn_acc_context()
                    .with_signer(&[&authority_seeds[..]]),
                destination_amount,
            )?;
        }

        msg!("Close destination account");
        // Close destination account
        token::close_account(
            ctx.accounts
                .into_close_destination_account_context()
                .with_signer(&[&authority_seeds[..]]),
        )?;

        Ok(())
    }

    pub fn swap(ctx: Context<Swap>) -> ProgramResult {
        let (vault_authority, vault_authority_bump) =
            Pubkey::find_program_address(&[VAULT_PDA_SEED], ctx.program_id);
        let authority_seeds = &[&VAULT_PDA_SEED[..], &[vault_authority_bump]];

        // Calculate swap amount
        let amount =
            ctx.accounts.vault.initial_amount / 100 * (ctx.accounts.vault.percentil as u64);

        let swap_instruction = amm_instructions::swap_base_in(
            ctx.accounts.swap_program.key,
            ctx.accounts.market_accs.amm_id.key,
            ctx.accounts.market_accs.amm_authority.key,
            ctx.accounts.market_accs.amm_open_orders.key,
            ctx.accounts.market_accs.amm_target_orders.key,
            ctx.accounts.market_accs.pool_coin_token_account.key,
            ctx.accounts.market_accs.pool_pc_token_account.key,
            ctx.accounts.dex_program.key,
            ctx.accounts.market_accs.market.key,
            ctx.accounts.market_accs.bids.key,
            ctx.accounts.market_accs.asks.key,
            ctx.accounts.market_accs.event_queue.key,
            ctx.accounts.market_accs.pool_coin_vault.key,
            ctx.accounts.market_accs.pool_pc_vault.key,
            ctx.accounts.market_accs.pool_vault_signer.key,
            ctx.accounts.deposit_account.to_account_info().key,
            ctx.accounts.destination_account.to_account_info().key,
            &vault_authority,
            amount,
            10,
        )
        .unwrap();

        let _ = invoke_signed(
            &swap_instruction,
            &[
                ctx.accounts.market_accs.amm_id.clone(),
                ctx.accounts.market_accs.amm_authority.clone(),
                ctx.accounts.market_accs.amm_open_orders.clone(),
                ctx.accounts.market_accs.amm_target_orders.clone(),
                ctx.accounts.market_accs.pool_coin_token_account.clone(),
                ctx.accounts.market_accs.pool_pc_token_account.clone(),
                ctx.accounts.dex_program.clone(),
                ctx.accounts.market_accs.market.clone(),
                ctx.accounts.market_accs.bids.clone(),
                ctx.accounts.market_accs.asks.clone(),
                ctx.accounts.market_accs.event_queue.clone(),
                ctx.accounts.market_accs.pool_coin_vault.clone(),
                ctx.accounts.market_accs.pool_pc_vault.clone(),
                ctx.accounts.market_accs.pool_vault_signer.clone(),
                ctx.accounts.deposit_account.to_account_info(),
                ctx.accounts.destination_account.to_account_info(),
                ctx.accounts.vault_authority.clone(),
            ],
            &[authority_seeds],
        );

        Ok(())
    }
}

#[derive(Accounts)]
pub struct InitializeTests {}

#[derive(Accounts)]
#[instruction(period: u8, percentil: u8, amount: u64, deposit_bump: u8, destination_bump: u8)]
pub struct InitializeVault<'info> {
    // Deposit token account
    #[account(
        init,
        seeds = [
                b"deposit",
                vault.to_account_info().key().as_ref(),
            ],
        bump,
        payer = authority,
        token::mint = deposit_mint,
        token::authority = authority)]
    pub deposit_account: Account<'info, TokenAccount>,

    // Destination token account
    #[account(
        init,
        seeds = [
                b"destination",
                vault.to_account_info().key().as_ref(),
            ],
        bump,
        payer = authority,
        token::mint = destination_mint,
        token::authority = authority)]
    pub destination_account: Account<'info, TokenAccount>,

    // Vault stores swap instructions
    #[account(init, payer = authority, space = 201)]
    pub vault: Box<Account<'info, Vault>>,

    #[account(mut)]
    pub authority: Signer<'info>,

    pub deposit_mint: Account<'info, Mint>,
    pub destination_mint: Account<'info, Mint>,
    #[account(mut)]
    pub owner_deposit_token_account: Account<'info, TokenAccount>,

    #[account(address = system_program::ID)]
    /// CHECK: error
    pub system_program: AccountInfo<'info>,
    /// CHECK: error
    pub token_program: AccountInfo<'info>,
    pub rent: Sysvar<'info, Rent>,
}

#[derive(Accounts)]
pub struct CloseVault<'info> {
    // get vault token accounts
    #[account(
        mut,
        seeds = [
                b"deposit",
                vault.to_account_info().key().as_ref(),
            ],
        bump = vault.deposit_bump,
    )]
    pub deposit_account: Account<'info, TokenAccount>,
    #[account(
        mut,
        seeds = [
                b"destination",
                vault.to_account_info().key().as_ref(),
            ],
        bump = vault.destination_bump,
    )]
    pub destination_account: Account<'info, TokenAccount>,

    #[account(
        mut,
        constraint = vault.authority == *authority.key,
    )]
    pub vault: Box<Account<'info, Vault>>,

    #[account(mut)]
    pub deposit_withdrawn_account: Account<'info, TokenAccount>,
    #[account(mut)]
    pub destination_withdrawn_account: Account<'info, TokenAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    /// CHECK: error
    pub vault_authority: AccountInfo<'info>,

    #[account(address = system_program::ID)]
    /// CHECK: error
    pub system_program: AccountInfo<'info>,
    /// CHECK: error
    pub token_program: AccountInfo<'info>,
    pub rent: Sysvar<'info, Rent>,
}

#[derive(Accounts)]
pub struct Swap<'info> {
    #[account(
        mut,
        seeds = [
                b"deposit",
                vault.to_account_info().key().as_ref(),
            ],
        bump = vault.deposit_bump,
    )]
    pub deposit_account: Box<Account<'info, TokenAccount>>,
    #[account(
        mut,
        seeds = [
                b"destination",
                vault.to_account_info().key().as_ref(),
            ],
        bump = vault.destination_bump,
    )]
    pub destination_account: Box<Account<'info, TokenAccount>>,
    /// CHECK: error
    pub vault_authority: AccountInfo<'info>,

    #[account(mut)]
    pub vault: Box<Account<'info, Vault>>,

    pub market_accs: MarketAccounts<'info>,

    // Programs.
    /// CHECK: error
    pub dex_program: AccountInfo<'info>,
    /// CHECK: error
    pub swap_program: AccountInfo<'info>,
    /// CHECK: error
    pub token_program: AccountInfo<'info>,
}

#[derive(Accounts, Clone)]
pub struct MarketAccounts<'info> {
    #[account(mut)]
    /// CHECK: error
    pub market: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub amm_id: AccountInfo<'info>,
    /// CHECK: error
    pub amm_authority: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub amm_open_orders: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub amm_target_orders: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub pool_coin_token_account: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub pool_pc_token_account: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub request_queue: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub event_queue: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub bids: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: error
    pub asks: AccountInfo<'info>,
    // Also known as the "base" currency. For a given A/B market,
    // this is the vault for the A mint.
    #[account(mut)]
    /// CHECK: error
    pub pool_coin_vault: AccountInfo<'info>,
    // Also known as the "quote" currency. For a given A/B market,
    // this is the vault for the B mint.
    #[account(mut)]
    /// CHECK: error
    pub pool_pc_vault: AccountInfo<'info>,
    // PDA owner of the DEX's token accounts for base + quote currencies.
    /// CHECK: error
    pub pool_vault_signer: AccountInfo<'info>,
}

// State
#[account]
#[derive(Default)]
pub struct Vault {
    // 8
    pub authority: Pubkey,        // 32
    pub deposit_mint: Pubkey,     // 32
    pub destination_mint: Pubkey, // 32
    pub period: u8,               // 8
    pub percentil: u8,            // 8
    pub deposit_bump: u8,         // 8
    pub destination_bump: u8,     // 8
    pub initial_amount: u64,      // 64
    pub is_active: bool,          // 1
}

// Utils
impl<'info> InitializeVault<'info> {
    fn into_transfer_to_deposit_pda_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, Transfer<'info>> {
        let cpi_accounts = Transfer {
            from: self.owner_deposit_token_account.to_account_info().clone(),
            to: self.deposit_account.to_account_info().clone(),
            authority: self.authority.to_account_info().clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }

    fn into_set_deposit_authority_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, SetAuthority<'info>> {
        let cpi_accounts = SetAuthority {
            account_or_mint: self.deposit_account.to_account_info().clone(),
            current_authority: self.authority.to_account_info().clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }

    fn into_set_destination_authority_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, SetAuthority<'info>> {
        let cpi_accounts = SetAuthority {
            account_or_mint: self.destination_account.to_account_info().clone(),
            current_authority: self.authority.to_account_info().clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }
}

impl<'info> CloseVault<'info> {
    fn into_transfer_deposit_to_authority_withdrawn_acc_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, Transfer<'info>> {
        let cpi_accounts = Transfer {
            from: self.deposit_account.to_account_info().clone(),
            to: self.deposit_withdrawn_account.to_account_info().clone(),
            authority: self.vault_authority.clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }

    fn into_transfer_destination_to_authority_withdrawn_acc_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, Transfer<'info>> {
        let cpi_accounts = Transfer {
            from: self.destination_account.to_account_info().clone(),
            to: self.destination_withdrawn_account.to_account_info().clone(),
            authority: self.vault_authority.clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }

    fn into_close_deposit_account_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, CloseAccount<'info>> {
        let cpi_accounts = CloseAccount {
            account: self.deposit_account.to_account_info().clone(),
            authority: self.vault_authority.clone(),
            destination: self.authority.to_account_info().clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }

    fn into_close_destination_account_context(
        &self,
    ) -> CpiContext<'_, '_, '_, 'info, CloseAccount<'info>> {
        let cpi_accounts = CloseAccount {
            account: self.destination_account.to_account_info().clone(),
            authority: self.vault_authority.clone(),
            destination: self.authority.to_account_info().clone(),
        };
        CpiContext::new(self.token_program.clone(), cpi_accounts)
    }
}

#[error]
pub enum ErrorCode {
    #[msg("The tokens being swapped must have different mints")]
    SwapTokensCannotMatch,
    #[msg("Slippage tolerance exceeded")]
    SlippageExceeded,
}
